import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const model = mongoose.model;

const User = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  diskSpace: { type: Number, default: 1024 ** 3 },
  usedSpace: { type: Number, default: 0 },
  avatar: { type: String, default: null },
  username: { type: String, default: null },
  files: [{ type: ObjectId, ref: "File" }],
  allowed: [{ type: ObjectId, ref: "User" }],
  usage: [{ type: ObjectId, ref: "User" }],
});

export default model("User", User);
