import express, { json } from "express";
import mongoose from "mongoose";
import config from "config";
import authRouter from "./routes/auth.routes.js";
import fileRouter from "./routes/file.routes.js";
import userRouter from "./routes/user.routes.js";
import cors from "./middleware/cors.middleware.js";
import fileUpload from "express-fileupload";

const app = express();
const PORT = config.get("serverPort");

app.use(fileUpload({}));
app.use(cors);
app.use(json());
app.use(express.static("static"));
app.use("/api/auth", authRouter);
app.use("/api/user", userRouter);
app.use("/api/files", fileRouter);

const start = async () => {
  try {
    await mongoose.connect(config.get("dbUrl"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
      console.log("Server was started on port ", PORT);
    });
  } catch (e) {
    console.log(e);
  }
};

start();
