import Router from "express";
import authMiddleware from "../middleware/auth.middleware.js";
import fileController from "../controllers/fileController.js";

const router = new Router();

router.post("", authMiddleware, fileController.createDir);
router.post("/upload", authMiddleware, fileController.uploadFile);
router.get("/download", authMiddleware, fileController.downloadFile);
router.get("", authMiddleware, fileController.getFiles);
router.delete("", authMiddleware, fileController.removeFile);
router.delete("/deletedir", authMiddleware, fileController.deleteDir);
router.patch("/avatar", authMiddleware, fileController.uploadAvatar);
router.delete("/deleteavatar", authMiddleware, fileController.deleteAvatar);

export default router;
