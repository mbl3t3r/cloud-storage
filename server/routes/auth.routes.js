import Router from "express";
import User from "../models/User.js";
import bcryptjs from "bcryptjs";
import { check, validationResult } from "express-validator";
import jwt from "jsonwebtoken";
import config from "config";
import authMiddleware from "../middleware/auth.middleware.js";

import fileService from "../services/fileService.js";
import File from "../models/File.js";

const { hash, compareSync } = bcryptjs;
const { sign } = jwt;

const router = new Router();

router.post(
  "/registration",
  [
    check("email", "Некорректный email").isEmail(),
    check(
      "password",
      "Пароль должен составлять не менее 3 и не более 12 символов"
    ).isLength({ min: 3, max: 12 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: errors.errors[0].msg });
      }
      const { email, password } = req.body;

      const candidate = await User.findOne({ email });

      if (candidate) {
        return res.status(409).json({
          message: "Пользователь с таким email уже существует",
        });
      }
      const hashPassword = await hash(password, 8);
      const user = new User({ email: email, password: hashPassword });
      await user.save();
      await fileService.createDir(new File({ user: user.id, name: "" }));

      return res.status(200).json({ message: "Пользователь создан успешно" });
    } catch (e) {
      return res.status(500).json({
        message: "Ошибка сервера. Попробуйте позже",
      });
    }
  }
);

router.post(
  "/login",
  [check("email", "Uncorrect email").isEmail()],

  async (req, res) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      if (!user) {
        return res.status(404).json({
          message: "Пользователь не найден",
        });
      }
      const isPassValid = compareSync(password, user.password);
      if (!isPassValid) {
        return res.status(400).json({ message: "Неверный пароль" });
      }
      const token = sign({ id: user.id }, config.get("secretKey"), {
        expiresIn: "1h",
      });

      return res.status(200).json({
        token,
        user: {
          username: user.username,
          id: user.id,
          email: user.email,
          diskSpace: user.diskSpace,
          usedSpace: user.usedSpace,
          avatar: user.avatar,
        },
      });
    } catch (e) {
      return res
        .status(500)
        .json({ message: "Ошибка сервера. Попробуйте позже" });
    }
  }
);

router.get("/auth", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.user.id });
    const token = sign({ id: user.id }, config.get("secretKey"), {
      expiresIn: "1h",
    });
    return res.status(200).json({
      token,
      user: {
        username: user.username,
        id: user.id,
        email: user.email,
        diskSpace: user.diskSpace,
        usedSpace: user.usedSpace,
        avatar: user.avatar,
      },
    });
  } catch (e) {
    return res
      .status(500)
      .json({ message: "Ошибка сервера. Попробуйте позже" });
  }
});

export default router;
