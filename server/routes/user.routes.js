import Router from "express";
import User from "../models/User.js";
import authMiddleware from "../middleware/auth.middleware.js";

const router = new Router();

router.patch("/username", authMiddleware, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.user.id });

    if (!user)
      return res.status(404).json({ message: "Пользователь не найден" });

    user.username = req.body.username;
    await user.save();
    return res.status(200).json(user);
  } catch (e) {
    return res
      .status(500)
      .json({ message: "Ошибка сервера. Попробуйте позже" });
  }
});

export default router;
