import fs from "fs";
import File from "../models/File.js";
import path from "path";

class FileService {
  createDir(file) {
    const root = path.resolve("./files");
    if (!fs.existsSync(root)) {
      fs.mkdirSync(root);
    }

    const filePath = path.join(
      path.resolve("./files"),
      `${file.user}`,
      `${file.path}`
    );

    return new Promise((resolve, reject) => {
      try {
        if (!fs.existsSync(filePath)) {
          fs.mkdirSync(filePath);
          return resolve({ message: "File was created" });
        } else {
          return reject({ message: "File already exist" });
        }
      } catch (e) {
        return reject({ message: `File error: ${e.message}` });
      }
    });
  }
}

export default new FileService();
