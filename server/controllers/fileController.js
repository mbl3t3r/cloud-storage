import fileService from "../services/fileService.js";
import User from "../models/User.js";
import File from "../models/File.js";
import fs from "fs";
import path from "path";
import { v4 as uuidv4 } from "uuid";

class FileController {
  async createDir(req, res) {
    try {
      const { name, type, parent } = req.body;
      const file = new File({ name, type, parent, user: req.user.id });
      const parentFile = await File.findOne({ _id: parent });

      if (!parentFile) {
        file.path = name;
        await fileService.createDir(file);
      } else {
        file.path = path.join(parentFile.path, file.name);
        await fileService.createDir(file);
        parentFile.childs.push(file._id);
        await parentFile.save();
      }
      await file.save();
      console.log("file ---> ", file);
      return res
        .status(200)
        .json({ message: "Директория успешно создана", data: file });
    } catch (e) {
      return res.status(400).json({ message: "Ошибка сервера" });
    }
  }

  async getFiles(req, res) {
    try {
      const files = await File.find({
        user: req.user.id,
        parent: req.query.parent,
      });

      return res.status(200).json(files);
    } catch (e) {
      return res.status(500).json({ message: `Ошибка при получении файлов` });
    }
  }

  async uploadFile(req, res) {
    try {
      const file = req.files.file;

      const parent = await File.findOne({
        user: req.user.id,
        _id: req.body.parent,
      });

      const user = await User.findOne({ _id: req.user.id });

      if (user.usedSpace + file.size > user.diskSpace) {
        return res.status(400).json({ message: "Недостаточно места" });
      }

      user.usedSpace += file.size;

      let filePath;
      if (parent) {
        filePath = path.join(
          path.resolve(),
          "files",
          `${user._id}`,
          `${parent.path}`,
          `${file.name}`
        );
      } else {
        filePath = path.join(
          path.resolve(),
          "files",
          `${user._id}`,
          `${file.name}`
        );
      }

      if (fs.existsSync(filePath)) {
        return res.status(400).json({
          message: `Файл с именем "${file.name}" уже существует`,
        });
      }

      file.mv(filePath);

      const type = file.name.split(".").pop();

      const mdbFile = new File({
        name: file.name,
        type,
        size: file.size,
        path: parent?.path,
        parent: parent?._id,
        user: user._id,
      });

      await mdbFile.save();

      if (parent) {
        parent.childs.push(mdbFile._id);
        await parent.save();
      }

      await user.save();

      return res.status(200).json({
        message: "Файл успешно загружен",
        file: mdbFile,
        user: user,
      });
    } catch (e) {
      return res.status(500).json({ message: "Ошибка при загрузке файла" });
    }
  }

  async downloadFile(req, res) {
    try {
      const file = await File.findOne({
        _id: req.query.id,
        user: req.user.id,
      });

      if (!file) {
        return res.status(404).json({ message: "Файл не найден" });
      }

      const filePath = path.join(
        path.resolve(),
        "files",
        `${req.user.id}`,
        `${file.path}`,
        `${file.name}`
      );

      if (fs.existsSync(filePath)) {
        return res.download(filePath);
      }
    } catch (e) {
      return res.status(500).json({ message: "Ошибка при скачивании файла" });
    }
  }

  async removeFile(req, res) {
    try {
      const file = await File.findOne({ _id: req.query.id, user: req.user.id });
      const user = await User.findOne({ _id: req.user.id });
      const parent = await File.findOne({ _id: file.parent });

      if (parent) {
        const removedChildIndex = parent.childs.indexOf(file._id);
        parent.childs.splice(removedChildIndex, 1);
        await parent.save();
      }

      if (!file) {
        return res.status(404).json({ message: "Файл не существует" });
      }

      const filePath = path.join(
        path.resolve(),
        "files",
        `${req.user.id}`,
        `${file.path}`,
        `${file.name}`
      );

      await File.deleteOne(file);
      if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
        user.usedSpace -= file.size;
        await user.save();

        return res.status(200).json({ message: "Файл удален", user: user });
      } else {
        return res.status(404).json({ message: "Файл не найден" });
      }
    } catch (e) {
      return res.status(500).json({ message: "Ошибка при удалении файла" });
    }
  }

  async deleteDir(req, res) {
    try {
      const dir = await File.findOne({ _id: req.query.id, user: req.user.id });
      const user = await User.findOne({ _id: req.user.id });

      if (!dir || !(dir.type === "dir"))
        return res.status(400).json({ message: "Директория не найдена" });
      if (!user)
        return res.status(400).json({ message: "Пользователь не найден" });

      const dirPath = path.join(
        path.resolve(),
        "files",
        `${user.id}`,
        `${dir.path}`
      );

      const totalChilds = [];
      let returnedSpace = 0;

      async function getChilds(root) {
        const childs = await File.find({ parent: root._id });

        for (const child of childs) {
          totalChilds.push(child._id);
          returnedSpace += child.size;
          await getChilds(child);
        }
      }

      await getChilds(dir);

      for (const child of totalChilds) {
        await File.deleteOne({ _id: child });
      }

      await File.deleteOne(dir);

      user.usedSpace -= returnedSpace;
      await user.save();

      if (fs.existsSync(dirPath)) {
        fs.rmdirSync(dirPath, { recursive: true });
      }

      return res
        .status(200)
        .json({ message: "Директория успешно удалена", user });
    } catch (e) {
      return res
        .status(500)
        .json({ message: "Ошибка при удалении директории" });
    }
  }

  async uploadAvatar(req, res) {
    try {
      const file = req.files.file;
      const user = await User.findById(req.user.id);

      if (!user)
        return res.status(404).json({ message: "Пользователь не найден" });

      if (!file) return res.status(400).json({ message: "Отсутствует файл" });

      if (user.avatar) {
        fs.unlinkSync(path.join(path.resolve(), "static", user.avatar));
      }

      const avatarName = uuidv4() + ".jpg";
      file.mv(path.join(path.resolve(), "static", avatarName));

      user.avatar = avatarName;
      await user.save();

      return res.status(200).json(user);
    } catch (e) {
      return res.status(500).json({ message: "Ошибка при загрузке аватара" });
    }
  }

  async deleteAvatar(req, res) {
    try {
      const user = await User.findById(req.user.id);

      if (!user)
        return res.status(404).json({ message: "Пользователь не найден" });

      fs.unlinkSync(path.join(path.resolve(), "static", user.avatar));
      user.avatar = null;

      await user.save();

      return res.status(200).json(user);
    } catch (e) {
      return res.status(500).json({ message: "Ошибка при удалении аватара" });
    }
  }
}

export default new FileController();
