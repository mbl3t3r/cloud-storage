import React, { useState } from "react";
import { setUsername } from "../../actions/user";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Button } from "antd";
import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { LeftOutlined, PlusOutlined } from "@ant-design/icons";
import defaultAvatar from "../../assets/images/user/user.png";
import { uploadAvatar } from "../../actions/file";
import styles from "./profile.module.scss";
import ErrorStack from "../../components/errorStack/ErrorStack";
import { logout } from "../../reducers/userReducer";

import { SERVER_URL } from "../../config";
import {
  addProfileError,
  removeAllErrors,
  removeProfileError,
} from "../../reducers/errorReducer";

const verificateUsername = (username) => {
  const key = /^[^\\u0000\\/:?"<>|]{3,20}$/;
  const candidate = username.trim();

  if (key.test(candidate)) return true;
  return false;
};

const Profile = () => {
  const dispatch = useDispatch();
  const [showUsername, setShowUsername] = useState(false);
  const [usr, setUsr] = useState("");

  const avatar = useSelector((state) => state.user.currentUser.avatar);
  const email = useSelector((state) => state.user.currentUser.email);
  const username = useSelector((state) => state.user.currentUser.username);
  const errors = useSelector((state) => state.errors.profile);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (verificateUsername(usr)) {
      dispatch(setUsername(usr.trim()));
      setUsr("");
      setShowUsername(false);
    } else {
      dispatch(addProfileError("Некорректный псевдоним"));
    }
  };

  const changeAvatarHandler = (e) => {
    const file = e.target.files[0];
    dispatch(uploadAvatar(file));
  };

  return (
    <div className={styles.profile}>
      <Link to="/" className={styles.back}>
        <LeftOutlined />
        Назад к хранилищу
      </Link>
      <div className={styles.content}>
        <h1 className={styles.title}>Профиль</h1>

        <div className={styles.avatarModule}>
          <div className={styles.currentAvatar}>
            <img
              src={avatar ? `${SERVER_URL}/` + avatar : defaultAvatar}
              alt=""
            />
          </div>
          <input
            id="Upload"
            onChange={(e) => changeAvatarHandler(e)}
            type="file"
            className={styles.input}
          ></input>
          <label className={styles.changerAvatar} htmlFor="Upload">
            <PlusOutlined />
            <span className={styles.newAvatarTitle}>Новый аватар</span>
          </label>
        </div>

        <div className={styles.info}>
          <span className={styles.key}>E-mail</span>
          <span className={styles.value}>{email}</span>

          <span className={styles.key}>Псевдоним</span>
          <span className={styles.value}>
            <button
              className={styles.showChangeUsername}
              onClick={() => setShowUsername(!showUsername)}
            >
              Указать
            </button>
            {showUsername && (
              <form
                className={styles.changeUsernameForm}
                onSubmit={handleSubmit}
              >
                <input
                  value={usr}
                  onChange={(e) => setUsr(e.target.value)}
                  type="text"
                  autoFocus
                  placeholder={username && username}
                />
                <button
                  classtype="submit"
                  value=""
                  className={styles.changeUsernameButton}
                >
                  {verificateUsername(usr) ? (
                    <i className={styles.verificateUsernameTrue}>
                      <CheckCircleOutlined />
                    </i>
                  ) : (
                    <i className={styles.verificateUsernameFalse}>
                      <CloseCircleOutlined />
                    </i>
                  )}
                </button>
              </form>
            )}
          </span>
        </div>

        <ul className={styles.actions}>
          <li>
            <Button
              danger
              onClick={() => {
                dispatch(removeAllErrors());
                dispatch(logout());
              }}
            >
              Выйти
            </Button>
          </li>
        </ul>
      </div>

      <ErrorStack errors={errors} remove={removeProfileError} />
    </div>
  );
};

export default Profile;
