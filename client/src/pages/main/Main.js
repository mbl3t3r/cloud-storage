import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Navbar from "../../components/navbar/Navbar";
import Storage from "../../components/storage/Storage";
import Upload from "../../components/storage/upload/Upload";
import Uploader from "../../components/storage/upload/uploader/Uploader";
import SpaceIndicator from "../../components/storage/spaceIndicator/SpaceIndicator";
import Profile from "../profile/Profile";
import styles from "./main.module.scss";

const Main = () => {
  return (
    <div className={styles.wrapper}>
      <Navbar />
      <div className={styles.containerWrapper}>
        <Switch>
          <Route path="/profile">
            <Profile />
          </Route>

          <Route path="/" exact>
            <div className={styles.panel}>
              <Upload />
              <SpaceIndicator />
              <Uploader />
            </div>
            <div className={styles.container}>
              <Storage />
            </div>
          </Route>

          <Redirect to="/" />
        </Switch>
      </div>
    </div>
  );
};

export default Main;
