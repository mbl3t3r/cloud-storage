import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Logo from "../../components/logo/Logo";
import Login from "../../components/buttons/login/Login";
import Register from "../../components/buttons/register/Register";

import Authorization from "../../components/authorization/Authorization";
import Registration from "../../components/registration/Registration";

import styles from "./start.module.scss";

const Start = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <div className={styles.header}>
          <Logo />
          <Route path="/">
            <div className={styles.buttons}>
              <Login className={styles.button} />
              <Register className={styles.button} />
            </div>
          </Route>
        </div>
        <Switch>
          <Route path="/login">
            <Authorization />
          </Route>
          <Route path="/register">
            <Registration />
          </Route>
          <Redirect to="/" />
        </Switch>
      </div>
    </div>
  );
};

export default Start;
