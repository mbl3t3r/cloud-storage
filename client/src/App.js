import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Start from "./pages/start/Start";
import Main from "./pages/main/Main";
import { auth } from "./actions/user";

const App = () => {
  const isAuth = useSelector((state) => state.user.isAuth);

  const dispatch = useDispatch();

  useEffect(() => dispatch(auth()));

  return !isAuth ? <Start /> : <Main />;
};

export default App;
