import dir from "../assets/images/filetypes/folder.png";
import def from "../assets/images/filetypes/default.png";
import img from "../assets/images/filetypes/image.png";
import vid from "../assets/images/filetypes/video.png";
import iso from "../assets/images/filetypes/iso.png";
import doc from "../assets/images/filetypes/doc.png";
import excel from "../assets/images/filetypes/excel.png";
import font from "../assets/images/filetypes/font-file.png";
import js from "../assets/images/filetypes/js-file.png";
import aud from "../assets/images/filetypes/mp3.png";
import pdf from "../assets/images/filetypes/pdf.png";
import powerpoint from "../assets/images/filetypes/powerpoint.png";
import psd from "../assets/images/filetypes/psd.png";
import svg from "../assets/images/filetypes/svg.png";
import txt from "../assets/images/filetypes/txt.png";
import zip from "../assets/images/filetypes/zip.png";
import css from "../assets/images/filetypes/css.png";

const dirs = new Map();

dirs.set("dir", dir);

dirs.set("jpeg, png, apng, avif, gif, jpg, webp", img);
dirs.set("doc, docx", doc);
dirs.set("css, scss, sass", css);
dirs.set("mp4, webm, avchd, flv, mov, wmv, mkv, avi", vid);
dirs.set("zip, rar, tar, arj, cab, lzh", zip);
dirs.set("mp3, flac, wav, ogg", aud);
dirs.set("ppt, pptx, pps, ppsx, pot, potx", powerpoint);

dirs.set("iso", iso);
dirs.set("xls", excel);
dirs.set("pdf", pdf);
dirs.set("otf", font);
dirs.set("txt", txt);
dirs.set("js", js);
dirs.set("psd", psd);
dirs.set("svg", svg);

export function setTypeIcon(type) {
  const t = type.toLowerCase();

  for (let [key, value] of dirs.entries()) {
    if (!key.includes(t)) continue;
    return value;
  }
  return def;
}
