export const sortFiles = (files, type) => {
  switch (type) {
    case null:
      return files;

    case "type": {
      const dirFiles = files
        .filter((el) => el.type === "dir")
        .sort((a, b) => a.name.localeCompare(b.name));
      const otherFiles = files.filter((el) => el.type !== "dir");

      return dirFiles.concat(
        otherFiles.sort((a, b) => a.type.localeCompare(b.type))
      );
    }

    case "name": {
      const dirFiles = files
        .filter((el) => el.type === "dir")
        .sort((a, b) => a.name.localeCompare(b.name));
      const otherFiles = files.filter((el) => el.type !== "dir");

      return dirFiles.concat(
        otherFiles.sort((a, b) => a.name.localeCompare(b.name))
      );
    }

    default:
      return files;
  }
};
