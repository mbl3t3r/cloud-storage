import axios from "axios";
import { SERVER_URL } from "../config";
import { setUser, addAllowedUser } from "../reducers/userReducer";
import { addAuthError, addProfileError } from "../reducers/errorReducer";

export const registration = (email, password) => {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/auth/registration`, SERVER_URL).toString();

      const response = await axios.post(url, {
        email,
        password,
      });

      if (response.status === 200) {
        dispatch(login(email, password));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addAuthError(message));
      } else {
        dispatch(addAuthError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
};

export const login = (email, password) => {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/auth/login`, SERVER_URL).toString();

      const response = await axios.post(url, {
        email,
        password,
      });

      if (response.status === 200) {
        localStorage.setItem("token", response.data.token);
        dispatch(setUser(response.data.user));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addAuthError(message));
      } else {
        dispatch(addAuthError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
};

export const auth = () => {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/auth/auth`, SERVER_URL).toString();

      const response = await axios.get(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });
      if (response.status === 200) {
        localStorage.setItem("token", response.data.token);
        dispatch(setUser(response.data.user));
      }
    } catch (e) {
      localStorage.removeItem("token");
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addAuthError(message));
      } else {
        dispatch(addAuthError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
};

export const setUsername = (username) => {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/user/username`, SERVER_URL).toString();

      const response = await axios.patch(
        url,
        {
          username,
        },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );

      if (response.status === 200) {
        dispatch(setUser(response.data));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addProfileError(message));
      } else {
        dispatch(addProfileError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
};

export function addAllowedUserAction(email) {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/user/addallowed`, SERVER_URL).toString();

      const response = await axios.post(
        url,
        { email },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );

      if (response.status === 200) {
        dispatch(addAllowedUser(email));
      }
    } catch (e) {
      const message = e.respnse?.data?.message;

      if (message) {
        dispatch(addProfileError(message));
      } else {
        dispatch(addProfileError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
}
