import axios from "axios";
import { SERVER_URL } from "../config";
import {
  setFiles,
  addFile,
  deleteFile,
  removeDir,
} from "../reducers/fileReducer";
import { setUser } from "../reducers/userReducer";
import {
  showUploader,
  appendUploadFile,
  removeUploadFile,
  changeUploadFile,
} from "../reducers/uploaderReducer";

import { addStorageError, addProfileError } from "../reducers/errorReducer";

export function getFiles(dirId) {
  return async (dispatch) => {
    try {
      const url = new URL(
        `/api/files${dirId ? "?parent=" + dirId : ""}`,
        SERVER_URL
      ).toString();

      const response = await axios.get(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });

      if (response.status === 200) {
        dispatch(setFiles(response.data));
      }
    } catch (e) {
      const message = e.response?.data?.message;
      if (message) {
        dispatch(addStorageError(message));
      } else {
        dispatch(addStorageError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
}

export function createDir(name, parent) {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/files`, SERVER_URL).toString();

      const response = await axios.post(
        url,
        {
          name: name,
          type: "dir",
          parent,
        },
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );

      if (response.status === 200) {
        dispatch(addFile(response.data.data));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addStorageError(message));
      } else {
        dispatch(addStorageError("Произошла непредвиденная ошибка"));
      }

      return e;
    }
  };
}

export function uploadFile(file, parent) {
  return async (dispatch) => {
    const uploadedFile = { name: file.name, progress: 0, id: Date.now() };

    try {
      const fileFormData = new FormData();
      fileFormData.append("file", file);

      if (parent) {
        fileFormData.append("parent", parent);
      }

      dispatch(showUploader());
      dispatch(appendUploadFile(uploadedFile));

      const url = new URL(`/api/files/upload`, SERVER_URL).toString();

      const response = await axios.post(url, fileFormData, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },

        onUploadProgress: (progressEvent) => {
          const totalLength = progressEvent.lengthComputable
            ? progressEvent.total
            : progressEvent.target.getResponseHeader("content-length") ||
              progressEvent.target.getResponseHeader(
                "x-decompressed-content-length"
              );

          if (totalLength) {
            uploadedFile.progress = Math.round(
              (progressEvent.loaded * 100) / totalLength
            );
            dispatch(changeUploadFile(uploadedFile));
          }
        },
      });
      if (response.status === 200) {
        await dispatch(addFile(response.data.file));
        await dispatch(setUser(response.data.user));
      }
    } catch (e) {
      const message = e.response?.data?.message;
      if (message) {
        dispatch(addStorageError(message));
        dispatch(removeUploadFile(uploadedFile.id));
      } else {
        dispatch(addStorageError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
}

export function downloadFile(file) {
  return async (dispatch) => {
    try {
      const url = new URL(
        `/api/files/download?id=${file._id}`,
        SERVER_URL
      ).toString();

      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });

      if (response.status === 200) {
        const blob = await response.blob();
        const downloadUrl = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = downloadUrl;
        link.download = file.name;
        document.body.appendChild(link);
        link.click();
        link.remove();
      } else {
        response.json().then((m) => dispatch(addStorageError(m.message)));
      }
    } catch (e) {
      dispatch(addStorageError("Произошла непредвиденная ошибка"));
      return e;
    }
  };
}

export function removeFile(file) {
  return async (dispatch) => {
    try {
      const url = new URL(`/api/files?id=${file._id}`, SERVER_URL).toString();

      const response = await axios.delete(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });

      if (response.status === 200) {
        await dispatch(deleteFile(file));
        await dispatch(setUser(response.data.user));
      }
    } catch (e) {
      const message = e.response?.data?.message?.message;

      if (message) {
        dispatch(addStorageError(message));
      }
      return e;
    }
  };
}

export function deleteDir(id) {
  return async (dispatch) => {
    try {
      const url = new URL(
        `/api/files/deletedir?id=${id}`,
        SERVER_URL
      ).toString();

      const response = await axios.delete(url, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });

      if (response.status === 200) {
        await dispatch(removeDir(id));
        await dispatch(setUser(response.data.user));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addStorageError(e.response.data.message));
      } else {
        dispatch(addStorageError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
}

export function uploadAvatar(file) {
  return async (dispatch) => {
    try {
      const fileFormData = new FormData();
      fileFormData.append("file", file);

      const url = new URL(`/api/files/avatar`, SERVER_URL).toString();

      const response = await axios.patch(url, fileFormData, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      });

      if (response.status === 200) {
        dispatch(setUser(response.data));
      }
    } catch (e) {
      const message = e.response?.data?.message;

      if (message) {
        dispatch(addProfileError(message));
      } else {
        dispatch(addProfileError("Произошла непредвиденная ошибка"));
      }
      return e;
    }
  };
}
