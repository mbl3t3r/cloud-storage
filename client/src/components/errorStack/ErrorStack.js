import React from "react";
import Error from "../error/Error";
import styles from "./errorstack.module.scss";

const ErrorStack = ({ errors, remove }) => {
  return (
    <div className={styles.stack}>
      {errors.map((error) => (
        <Error
          message={error.message}
          id={error.id}
          key={error.id}
          remove={remove}
        />
      ))}
    </div>
  );
};

export default ErrorStack;
