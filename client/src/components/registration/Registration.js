import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Button } from "antd";
import { registration } from "../../actions/user";
import { removeAuthError } from "../../reducers/errorReducer";
import ErrorStack from "../errorStack/ErrorStack";

import styles from "./registration.module.scss";

const Registration = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    dispatch(registration(values.email, values.password));
  };

  const errors = useSelector((state) => state.errors.auth);

  return (
    <div className={styles.register}>
      <h1>Регистрация</h1>
      <div className={styles.description}>
        <h2>
          Пожалуйста, зарегистрируйтесь, чтобы пользоваться облачным хранилищем
        </h2>
      </div>
      <Form
        form={form}
        name="register"
        className="ant-form-vertical"
        onFinish={onFinish}
        scrollToFirstError
      >
        <Form.Item
          name="email"
          className={styles.item}
          label="E-mail"
          rules={[
            {
              type: "email",
              message: "Введите корректный E-mail",
            },
            {
              required: true,
              message: "Введите свой E-mail",
            },
          ]}
        >
          <Input autoComplete="true" />
        </Form.Item>

        <Form.Item
          name="password"
          className={styles.item}
          label="Пароль"
          rules={[
            {
              required: true,
              message: "Введите пароль",
            },
          ]}
          hasFeedback
        >
          <Input.Password autoComplete="true" />
        </Form.Item>

        <Form.Item
          name="confirm"
          className={styles.item}
          label="Повторите пароль"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Повторите пароль",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
              },
            }),
          ]}
        >
          <Input.Password autoComplete="true" />
        </Form.Item>

        <Form.Item className={styles.item}>
          <Button type="primary" htmlType="submit" className={styles.btn}>
            Регистрация
          </Button>
        </Form.Item>
      </Form>

      <ErrorStack errors={errors} remove={removeAuthError} />
    </div>
  );
};

export default Registration;
