import React from "react";
import { Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../actions/user";

import { removeAuthError } from "../../reducers/errorReducer";
import ErrorStack from "../errorStack/ErrorStack";

import styles from "./authorization.module.scss";

const Authorization = () => {
  const [form] = Form.useForm();

  const isAuth = useSelector((state) => state.user.isAuth);
  const errors = useSelector((state) => state.errors.auth);

  const dispatch = useDispatch();

  const onFinish = async (values) => {
    dispatch(login(values.username, values.password));

    if (!isAuth) {
      form.resetFields();
    }
  };

  return (
    <div className={styles.auth}>
      <h1>Авторизация</h1>
      <div className={styles.description}>
        <h2>Войдите, чтобы пользоваться облачным хранилищем</h2>
      </div>
      <Form
        form={form}
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          className={styles.item}
          rules={[
            { required: true, message: "Пожалуйста, введите вашу почту" },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Имя"
            autoComplete="true"
          />
        </Form.Item>
        <Form.Item
          name="password"
          className={styles.item}
          rules={[{ required: true, message: "Пожалуйста, введите пароль" }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Пароль"
            autoComplete="true"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Войти
          </Button>
        </Form.Item>
      </Form>

      <ErrorStack errors={errors} remove={removeAuthError} />
    </div>
  );
};

export default Authorization;
