import React from "react";
import Logo from "../logo/Logo";
import Usermenu from "../usermenu/Usermenu";

import styles from "./navbar.module.scss";

const Navbar = () => {
  return (
    <div className={styles.navbar}>
      <div className={styles.container}>
        <Logo />
        <Usermenu />
      </div>
    </div>
  );
};

export default Navbar;
