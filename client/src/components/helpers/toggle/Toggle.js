import React from "react";
import styles from "./toggle.module.scss";

const Toggle = ({ toggleAction }) => {
  return <div className={styles.toggle} onClick={toggleAction}></div>;
};

export default Toggle;
