import React, { useState } from "react";
import { useSelector } from "react-redux";
import Menu from "../menu/Menu";
import defaultAvatar from "../../assets/images/user/user.png";
import Toggle from "../helpers/toggle/Toggle";
import { SERVER_URL } from "../../config";
import { CSSTransition } from "react-transition-group";

import fade from "./transitions.scss";
import styles from "./usermenu.module.scss";

const Usermenu = () => {
  const [showMenu, setShowMenu] = useState(false);
  const toggleMenu = () => setShowMenu(!showMenu);

  const username = useSelector((state) => state.user.currentUser.username);
  const email = useSelector((state) => state.user.currentUser.email);
  const avatar = useSelector((state) => state.user.currentUser.avatar);

  return (
    <div className={styles.userAuth} onClick={toggleMenu}>
      <button className={styles.user} onClick={toggleMenu} />

      <div className={styles.username}>{username ? username : email}</div>
      <div className={styles.avatar}>
        {
          <img
            src={avatar ? `${SERVER_URL}/` + avatar : defaultAvatar}
            alt="avatar"
          />
        }
      </div>

      {showMenu && (
        <CSSTransition
          in={showMenu}
          timeout={200}
          classNames={fade}
          unmountOnExit
        >
          <>
            <Menu />
            <Toggle toggleAction={toggleMenu} />
          </>
        </CSSTransition>
      )}
    </div>
  );
};

export default Usermenu;
