import React from "react";
import { useSelector } from "react-redux";
import File from "./file/File";
import { sortFiles } from "../../../utils/sortFiles";
import { Empty } from "antd";

import styles from "./fileList.module.scss";

const FileList = () => {
  const files = useSelector((state) => state.files.files);
  const type = useSelector((state) => state.files.sort);

  return files.length > 0 ? (
    <div className={styles.fileList}>
      <div className={styles.content}>
        {sortFiles(files, type).map((file) => {
          return <File file={file} key={file._id} />;
        })}
      </div>
    </div>
  ) : (
    <div className={styles.empty}>
      <Empty
        image={Empty.PRESENTED_IMAGE_SIMPLE}
        description={<span>Файлов пока нет</span>}
      />
    </div>
  );
};

export default FileList;
