import React from "react";
import { useDispatch } from "react-redux";
import { downloadFile, removeFile } from "../../../../../actions/file";
import cn from "classnames";
import { CloudDownloadOutlined, DeleteOutlined } from "@ant-design/icons";

import styles from "./fileActions.module.scss";

const FileActions = ({ className, file }) => {
  const dispatch = useDispatch();

  const downloadHandler = (event) => {
    event.stopPropagation();
    dispatch(downloadFile(file));
  };

  const removeHandler = () => {
    dispatch(removeFile(file));
  };
  return (
    <div className={cn(styles.action, className)}>
      <button className={styles.download} onClick={downloadHandler}>
        <CloudDownloadOutlined />
      </button>
      <button className={styles.delete} onClick={removeHandler}>
        <DeleteOutlined />
      </button>
    </div>
  );
};

export default FileActions;
