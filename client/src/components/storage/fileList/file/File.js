import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setCurrentDir } from "../../../../reducers/fileReducer";
import { pushDirStack } from "../../../../reducers/fileReducer";
import FileActions from "./fileActions/FileActions";
import { setTypeIcon } from "../../../../utils/setTypeIcon";

import styles from "./file.module.scss";

const File = ({ file }) => {
  const dispatch = useDispatch();
  const currentDir = useSelector((state) => state.files.current);

  const openDirHandler = (file) => {
    if (file.type === "dir") {
      dispatch(pushDirStack({ current: currentDir, name: file.name }));
      dispatch(setCurrentDir(file._id));
    }
  };

  return (
    <div
      className={styles.file}
      title={`Имя: ${file.name}\nТип: ${file.type}\nРазмер: ${(
        file.size /
        1024 /
        1024
      ).toFixed(2)}МБ`}
    >
      <div className={styles.image} onClick={() => openDirHandler(file)}>
        <img src={setTypeIcon(file.type)} alt="" />
      </div>

      {!(file.type === "dir") ? (
        <>
          <div className={styles.name}>{file.name}</div>
          <FileActions className={styles.action} file={file} />
        </>
      ) : (
        <div className={styles.dirName}>{file.name}</div>
      )}
    </div>
  );
};

export default File;
