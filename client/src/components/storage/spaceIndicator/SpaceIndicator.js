import React from "react";
import { useSelector } from "react-redux";
import { Progress } from "antd";
import styles from "./spaceIndicator.module.scss";

const bytesToGBytes = (bytes) => {
  return (bytes / 1024 / 1024 / 1024).toFixed(1);
};

const bytesToMBytes = (bytes) => {
  return (bytes / 1024 / 1024).toFixed(2);
};

const SpaceIndicator = () => {
  const usedSpace = useSelector((state) => state.user.currentUser.usedSpace);
  const availableSpace = useSelector(
    (state) => state.user.currentUser.diskSpace
  );

  const space = {
    availableGb: bytesToGBytes(availableSpace),
    usedGb: bytesToGBytes(usedSpace),
    usedMb: bytesToMBytes(usedSpace),
    busySpaceProcent: ((usedSpace / availableSpace) * 100).toFixed(0),
  };

  return (
    <div className={styles.indicator} title={space.usedMb + "МБ"}>
      <Progress type="circle" percent={space.busySpaceProcent} />
      <div className={styles.info}>
        <span className={styles.label}>Занято</span>
        <span className={styles.space}>
          {space.usedGb + "Gb"} / {space.availableGb + "Gb"}
        </span>
      </div>
    </div>
  );
};

export default SpaceIndicator;
