import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { uploadFile } from "../../../actions/file";
import { addStorageError } from "../../../reducers/errorReducer";

import styles from "./upload.module.scss";

const UploadBtn = () => {
  const dispatch = useDispatch();

  const currentDir = useSelector((state) => state.files.current);

  const diskSpace = useSelector((state) => state.user.currentUser.diskSpace);
  const usedSpace = useSelector((state) => state.user.currentUser.usedSpace);

  const fileUploadHandler = async (event) => {
    const files = [...event.target.files];

    for (const file of files) {
      if (file.size + usedSpace <= diskSpace) {
        await dispatch(uploadFile(file, currentDir));
      } else {
        dispatch(addStorageError("Недостаточно места"));
      }
    }
  };

  return (
    <div className={styles.upload}>
      <label className={styles.uploadBtn} htmlFor="uploadFile">
        Загрузить
      </label>
      <input
        className={styles.input}
        multiple={true}
        type="file"
        id="uploadFile"
        onChange={(event) => fileUploadHandler(event)}
      />
    </div>
  );
};

export default UploadBtn;
