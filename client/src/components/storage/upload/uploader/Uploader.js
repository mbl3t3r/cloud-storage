import React from "react";
import { CloseCircleOutlined } from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";
import {
  hideUpploader,
  removeUploadFile,
} from "../../../../reducers/uploaderReducer";

import styles from "./uploader.module.scss";

const Uploader = () => {
  const dispatch = useDispatch();

  const files = useSelector((state) => state.uploader.files);
  const toggle = useSelector((state) => state.uploader.isVisible);

  return (
    toggle &&
    files.length > 0 && (
      <div className={styles.uploader}>
        <button
          className={styles.closeButton}
          onClick={() => dispatch(hideUpploader())}
        >
          <CloseCircleOutlined />
        </button>
        {files.map((file) => (
          <div className={styles.file} key={file.id}>
            <div className={styles.name}>{file.name}</div>
            <div className={styles.progress}>
              <div className={styles.percent}>{file.progress}%</div>
              <div
                className={styles.progressBar}
                style={{ width: file.progress + "%" }}
              ></div>
            </div>
            <button
              className={styles.closeFile}
              onClick={() => dispatch(removeUploadFile(file.id))}
            ></button>
          </div>
        ))}
      </div>
    )
  );
};

export default Uploader;
