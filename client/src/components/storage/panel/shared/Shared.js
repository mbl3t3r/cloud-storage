import React, { useRef } from "react";
import { useState } from "react";
import share from "./img/share.png";
import Space from "./space";
import s from "./shared.module.scss";
import Toggle from "../../../helpers/toggle/Toggle";

const Shared = () => {
  const [show, setShow] = useState(false);
  const user = useRef(null);

  const showHandler = () => {
    setShow(!show);
  };

  const addUserHandler = () => {
    console.log(user.current.value);
    // логика экшна
    user.current.value = "";
  };

  return (
    <div className={s.shared}>
      <div className={s.icon} onClick={showHandler}>
        <img src={share} alt="" />
      </div>
      {show && (
        <>
          <div className={s.menu}>
            <div className={s.allow}>
              <div className={s.label}>Добавить пользователя</div>
              <div className={s.input}>
                <input type="text" placeholder="Введите email" ref={user} />
                <div className={s.ok} onClick={addUserHandler}>
                  <span>✔</span>
                </div>
              </div>

              <Space email="testdddd@ex.test" type="allow" />
              <Space email="test@example.test" type="allow" />
            </div>
            <br />
            <div className={s.usage}>
              <div className={s.label}>Используемые хранилища</div>
              <Space email="test1@example.test" type="usage" />
              <Space email="test2@example.test" type="usage" />
              <Space email="test3@example.test" type="usage" />
            </div>
          </div>
          {/* <Toggle toggleAction={showHandler} /> */}
        </>
      )}
    </div>
  );
};

export default Shared;
