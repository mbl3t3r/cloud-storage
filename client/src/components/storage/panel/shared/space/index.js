import React from "react";
import remove from "./img/remove.png";
import cn from "classnames";
import s from "./index.module.scss";

/**
 * @type {string} тип: 'allow' - кому позволено управлять, 'usage' - управляемые
 */

const Space = ({ email, type }) => {
  return (
    <div className={s.space}>
      <div className={cn(s.email, type === "usage" ? s.usage : s.allow)}>
        {email}
      </div>
      <div className={s.control}>
        <div className={s.remove}>
          <img src={remove} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Space;
