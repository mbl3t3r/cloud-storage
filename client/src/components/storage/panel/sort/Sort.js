import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  DownOutlined,
  UpOutlined,
  SortAscendingOutlined,
} from "@ant-design/icons";
import Toggle from "../../../helpers/toggle/Toggle";
import { setSort } from "../../../../reducers/fileReducer";

import styles from "./sort.module.scss";

const Sort = () => {
  const dispatch = useDispatch();
  const [showPanel, setShowPanel] = useState(false);
  const current = useSelector((state) => state.files.sort);

  const btnHandler = () => {
    setShowPanel(!showPanel);
  };

  return (
    <div className={styles.sortWrapper}>
      <div className={styles.sort}>
        <button className={styles.btn} onClick={btnHandler} />
        <i>
          <SortAscendingOutlined />
        </i>
        <div className={styles.typeName}>
          <span>
            {current
              ? current === "type"
                ? "По типу"
                : "По имени"
              : "Не сортировать"}
          </span>
          {showPanel ? <UpOutlined /> : <DownOutlined />}
        </div>
      </div>
      {showPanel && (
        <>
          <ul className={styles.panel}>
            <li>
              <button
                onClick={() => {
                  dispatch(setSort(null));
                  setShowPanel(false);
                }}
              />
              Не сортировать
            </li>
            <li>
              <button
                onClick={() => {
                  dispatch(setSort("type"));
                  setShowPanel(false);
                }}
              />
              По типу
            </li>
            <li>
              <button
                onClick={() => {
                  dispatch(setSort("name"));
                  setShowPanel(false);
                }}
              />
              По имени
            </li>
          </ul>
          <Toggle toggleAction={() => setShowPanel(false)} />
        </>
      )}
    </div>
  );
};

export default Sort;
