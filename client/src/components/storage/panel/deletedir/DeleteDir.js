import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteDir } from "../../../../actions/file";
import { DeleteOutlined } from "@ant-design/icons";
import { setCurrentDir } from "../../../../reducers/fileReducer";

import styles from "./deletedir.module.scss";

const DeleteDir = () => {
  const dispatch = useDispatch();
  const currentDir = useSelector((state) => state.files.current);
  const dirStack = useSelector((state) => state.files.dirStack);

  const deleteDirHandler = () => {
    dispatch(deleteDir(currentDir));

    const backDirId = dirStack.pop();
    dispatch(setCurrentDir(backDirId.current));
  };

  return (
    <div className={styles.deleteDir}>
      <button onClick={deleteDirHandler}></button>
      <DeleteOutlined />
    </div>
  );
};

export default DeleteDir;
