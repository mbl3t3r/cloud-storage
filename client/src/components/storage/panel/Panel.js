import React from "react";
import { useDispatch, useSelector } from "react-redux";
import CreateDir from "./createdir/CreateDir";
import { setCurrentDir } from "../../../reducers/fileReducer";
import Sort from "./sort/Sort";
import DeleteDir from "./deletedir/DeleteDir";
import Shared from "./shared/Shared";

import styles from "./panel.module.scss";

const Panel = () => {
  const dirStack = useSelector((state) => state.files.dirStack);
  const currentDir = useSelector((state) => state.files.current);
  const dispatch = useDispatch();

  const backHandler = () => {
    const backDirId = dirStack.pop();
    dispatch(setCurrentDir(backDirId.current));
  };

  const path = useSelector((state) => state.files.dirStack);

  return (
    <div className={styles.panel}>
      {dirStack.length > 0 && (
        <>
          <button
            className={styles.backDirButton}
            onClick={() => backHandler()}
          >
            Назад
          </button>
          <div className={styles.path}>
            {path.map((el) => (
              <span key={el.current}>{el.name}/</span>
            ))}
          </div>
        </>
      )}
      <div className={styles.tools}>
        <Sort />
        <CreateDir />
        {currentDir && <DeleteDir />}
      </div>
      {!currentDir && <Shared />}
    </div>
  );
};

export default Panel;
