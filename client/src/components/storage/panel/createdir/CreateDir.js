import React, { useState } from "react";
import { createDir } from "../../../../actions/file";
import { FolderAddOutlined, PlusOutlined } from "@ant-design/icons";
import Toggle from "../../../helpers/toggle/Toggle";
import styles from "./createdir.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { addStorageError } from "../../../../reducers/errorReducer";

const verificateDirName = (dirName) => {
  const key = /^[^\\u0000\\/:?"<>|]{1,15}$/;
  const candidate = dirName.trim();

  if (key.test(candidate)) return true;
  return false;
};

const CreateDir = () => {
  const dispatch = useDispatch();

  const parentDirId = useSelector((state) => state.files.current);
  const [name, setName] = useState("");
  const [show, setShow] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (verificateDirName(name)) {
      dispatch(createDir(name.trim(), parentDirId));
      setShow(false);
      setName("");
    } else {
      dispatch(addStorageError("Недопустимое имя директории"));
      setShow(false);
      setName("");
    }
  };

  const toggleAddFolder = () => {
    setShow(!show);
  };

  return (
    <>
      <div className={styles.addFolderWrapper}>
        <button className={styles.add} onClick={() => setShow(!show)}>
          <FolderAddOutlined />
        </button>

        {show && (
          <>
            <form onSubmit={handleSubmit} className={styles.form}>
              <input
                placeholder="Название папки..."
                className={styles.name}
                value={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                autoFocus
              />
              <button className={styles.add} classtype="submit" value="">
                <PlusOutlined />
              </button>
            </form>
            <Toggle toggleAction={toggleAddFolder} />
          </>
        )}
      </div>
    </>
  );
};

export default CreateDir;
