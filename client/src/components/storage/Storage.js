import React, { useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import Panel from "./panel/Panel";
import FileList from "./fileList/FileList";
import ErrorStack from "../errorStack/ErrorStack";
import { removeStorageError } from "../../reducers/errorReducer";

import styles from "./storage.module.scss";
import { getFiles } from "../../actions/file";
import { uploadFile } from "../../actions/file";
import { addStorageError } from "../../reducers/errorReducer";

const Storage = () => {
  const [draggable, setDraggable] = useState(false);

  const dispatch = useDispatch();
  const currentDir = useSelector((state) => state.files.current);

  const diskSpace = useSelector((state) => state.user.currentUser.diskSpace);
  const usedSpace = useSelector((state) => state.user.currentUser.usedSpace);

  const errors = useSelector((state) => state.errors.storage);

  useEffect(() => {
    dispatch(getFiles(currentDir));
  }, [currentDir, dispatch]);

  const dragEnterHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setDraggable(true);
  };

  const dragLeaveHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setDraggable(false);
  };

  const dropHandler = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const files = [...event.dataTransfer.files];
    files.forEach((file) => {
      if (file.size + usedSpace <= diskSpace) {
        dispatch(uploadFile(file, currentDir));
      } else {
        dispatch(addStorageError("Недостаточно места"));
      }
    });
    setDraggable(false);
  };

  return (
    <div
      className={styles.storage}
      onDragEnter={dragEnterHandler}
      onDragLeave={dragLeaveHandler}
      onDragOver={dragEnterHandler}
    >
      {draggable ? (
        <div
          className={styles.draggable}
          onDragEnter={dragEnterHandler}
          onDragLeave={dragLeaveHandler}
          onDragOver={dragEnterHandler}
          onDrop={dropHandler}
        >
          Перетащите файлы сюда
        </div>
      ) : (
        <>
          <Panel />
          <div className={styles.content}>
            <FileList />
          </div>
          <ErrorStack errors={errors} remove={removeStorageError} />
        </>
      )}
    </div>
  );
};

export default Storage;
