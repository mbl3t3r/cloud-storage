import React from "react";
import { Link } from "react-router-dom";
import { UserOutlined, LogoutOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { logout } from "../../reducers/userReducer";
import { removeAllErrors } from "../../reducers/errorReducer";

import styles from "./menu.module.scss";

const Menu = () => {
  const dispatch = useDispatch();

  return (
    <div className={styles.menu}>
      <ul>
        <li>
          <Link className={styles.button} to="/profile">
            <UserOutlined />
            <span className={styles.fieldName}>Профиль</span>
          </Link>
        </li>
        <li>
          <button
            type="text"
            className={styles.button}
            onClick={() => {
              dispatch(removeAllErrors());
              dispatch(logout());
            }}
          >
            <LogoutOutlined />
            <span className={styles.fieldName}>Выйти</span>
          </button>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
