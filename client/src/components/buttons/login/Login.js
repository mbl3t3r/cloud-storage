import { Link } from "react-router-dom";
import { Button } from "antd";
import cn from "classnames";

import styles from "./login.module.scss";

const Login = (props) => {
  return (
    <div className={styles.login}>
      <Link to="/login">
        <Button className={cn("auth-btn", props.className)} type="primary">
          Вход
        </Button>
      </Link>
    </div>
  );
};

export default Login;
