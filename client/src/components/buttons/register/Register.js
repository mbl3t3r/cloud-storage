import { Link } from "react-router-dom";
import { Button } from "antd";
import cn from "classnames";

import styles from "./register.module.scss";

const Register = (props) => {
  return (
    <div className={styles.register}>
      <Link to="/register">
        <Button className={cn("auth-btn", props.className, styles.reg)} ghost>
          Регистрация
        </Button>
      </Link>
    </div>
  );
};

export default Register;
