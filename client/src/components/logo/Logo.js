import { Link } from "react-router-dom";
import logo from "../../assets/images/logo.png";
import styles from "./logo.module.scss";

const Logo = () => {
  return (
    <div className={styles.logo}>
      <Link to="/">
        <div className={styles.logotype}>
          <img className={styles.img} src={logo} alt="Cloud" />
          <div className={styles.name}>Cloudy</div>
        </div>
      </Link>
    </div>
  );
};

export default Logo;
