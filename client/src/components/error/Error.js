import React from "react";
import { useDispatch } from "react-redux";
import { CloseCircleOutlined } from "@ant-design/icons";
import styles from "./error.module.scss";

const Error = ({ message, id, remove }) => {
  const dispatch = useDispatch();

  const closeErrorHandler = () => {
    dispatch(remove(id));
  };

  return (
    <div className={styles.error}>
      {message}
      <button className={styles.btn} onClick={closeErrorHandler}>
        <CloseCircleOutlined />
      </button>
    </div>
  );
};

export default Error;
