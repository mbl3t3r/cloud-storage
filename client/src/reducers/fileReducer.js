const SET_FILES = "SET_FILES";
const SET_CURRENT_DIR = "SET_CURRENT_DIR";
const ADD_FILES = "ADD_FILES";
const PUSH_DIR_STACK = "PUSH_DIR_STACK";
const REMOVE_FILE = "REMOVE_FILE";
const REMOVE_DIR = "REMOVE_DIR";
const SET_SORT = "SET_SORT";

const defaultState = {
  files: [],
  current: null,
  dirStack: [],
  sort: "type",
  allowed: [],
};

export const fileReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_FILES:
      return {
        ...state,
        files: action.payload,
      };

    case SET_CURRENT_DIR: {
      return {
        ...state,
        current: action.payload,
      };
    }

    case ADD_FILES: {
      const files = state.files;

      return {
        ...state,
        files: [...files, action.payload],
      };
    }

    case PUSH_DIR_STACK: {
      return {
        ...state,
        dirStack: [...state.dirStack, action.payload],
      };
    }

    case REMOVE_FILE: {
      const files = state.files.slice();
      const rmFile = files.find((el) => el._id === action.payload._id);
      const index = files.indexOf(rmFile);
      files.splice(index, 1);
      return {
        ...state,
        files: [...files],
      };
    }

    case REMOVE_DIR: {
      const files = state.files.slice();
      const rmDir = files.find((el) => el._id === action.payload);
      const index = files.indexOf(rmDir);
      files.splice(index, 1);
      return {
        ...state,
        files: [...files],
      };
    }

    case SET_SORT: {
      return {
        ...state,
        sort: action.payload,
      };
    }

    default:
      return state;
  }
};

export const setFiles = (files) => ({ type: SET_FILES, payload: files });
export const setCurrentDir = (dir) => ({ type: SET_CURRENT_DIR, payload: dir });
export const addFile = (file) => ({ type: ADD_FILES, payload: file });
export const pushDirStack = (dir) => ({ type: PUSH_DIR_STACK, payload: dir });
export const deleteFile = (file) => ({ type: REMOVE_FILE, payload: file });
export const removeDir = (id) => ({ type: REMOVE_DIR, payload: id });
export const setSort = (type) => ({ type: SET_SORT, payload: type });
