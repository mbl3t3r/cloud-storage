import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { fileReducer } from "./fileReducer";
import { userReducer } from "./userReducer";
import { errorReducer } from "./errorReducer";
import { uploaderReducer } from "./uploaderReducer";

const rootReducer = combineReducers({
  files: fileReducer,
  user: userReducer,
  errors: errorReducer,
  uploader: uploaderReducer,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
