const SET_USER = "SET_USER";
const LOGOUT = "LOGOUT";
const ADD_ALLOWED_USER = "ADD_ALLOWED_USER";

const defaultState = {
  currentUser: {},
  isAuth: false,
};

export const userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        currentUser: action.payload,
        isAuth: true,
      };
    case LOGOUT:
      localStorage.removeItem("token");
      return {
        ...state,
        currentUser: {},
        isAuth: false,
      };

    case ADD_ALLOWED_USER: {
      return {
        ...state,
        allowed: [...state.allowed, action.payload],
      };
    }
    default:
      return state;
  }
};

export const setUser = (user) => ({ type: SET_USER, payload: user });
export const logout = () => ({ type: LOGOUT });
export const addAllowedUser = (email) => ({
  type: ADD_ALLOWED_USER,
  payload: email,
});
