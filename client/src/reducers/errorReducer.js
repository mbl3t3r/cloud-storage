const ADD_AUTH_ERROR = "ADD_AUTH_ERROR";
const REMOVE_AUTH_ERROR = "REMOVE_AUTH_ERROR";

const ADD_STORAGE_ERROR = "ADD_STORAGE_ERROR";
const REMOVE_STORAGE_ERROR = "REMOVE_STORAGE_ERROR";

const ADD_PROFILE_ERROR = "ADD_PROFILE_ERROR";
const REMOVE_PROFILE_ERROR = "REMOVE_PROFILE_ERROR";

const REMOVE_ALL_ERRORS = "REMOVE_ALL_ERRORS";

const defaultState = {
  auth: [],
  storage: [],
  profile: [],
};

export const errorReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_AUTH_ERROR: {
      return {
        ...state,
        auth: [
          ...state.auth,
          { id: state.auth.length + 1, message: action.payload },
        ],
      };
    }

    case REMOVE_AUTH_ERROR: {
      return {
        ...state,
        auth: state.auth.filter((err) => err.id !== action.payload),
      };
    }

    case ADD_STORAGE_ERROR: {
      return {
        ...state,
        storage: [
          ...state.storage,
          { id: state.storage.length + 1, message: action.payload },
        ],
      };
    }

    case REMOVE_STORAGE_ERROR: {
      return {
        ...state,
        storage: state.storage.filter((err) => err.id !== action.payload),
      };
    }

    case ADD_PROFILE_ERROR: {
      return {
        ...state,
        profile: [
          ...state.profile,
          { id: state.profile.length + 1, message: action.payload },
        ],
      };
    }

    case REMOVE_PROFILE_ERROR: {
      return {
        ...state,
        profile: state.profile.filter((err) => err.id !== action.payload),
      };
    }

    case REMOVE_ALL_ERRORS: {
      return {
        ...state,
        auth: [],
        storage: [],
        profile: [],
      };
    }

    default:
      return state;
  }
};

export const addAuthError = (message) => ({
  type: ADD_AUTH_ERROR,
  payload: message,
});
export const removeAuthError = (id) => ({
  type: REMOVE_AUTH_ERROR,
  payload: id,
});

export const addStorageError = (message) => ({
  type: ADD_STORAGE_ERROR,
  payload: message,
});
export const removeStorageError = (id) => ({
  type: REMOVE_STORAGE_ERROR,
  payload: id,
});

export const addProfileError = (message) => ({
  type: ADD_PROFILE_ERROR,
  payload: message,
});
export const removeProfileError = (id) => ({
  type: REMOVE_PROFILE_ERROR,
  payload: id,
});

export const removeAllErrors = () => ({ type: REMOVE_ALL_ERRORS });
