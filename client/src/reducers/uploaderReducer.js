const SHOW_UPPLOADER = "TOGGLE_VISIBLE";
const HIDE_UPLOADER = "HIDE_UPLOADER";

const APPEND_UPLOAD_FILE = "APPEND_UPLOAD_FILE";
const REMOVE_UPLOAD_FILE = "REMOVE_UPLOAD_FILE";

const CHANGE_UPLOAD_FILE = "CHANGE_UPLOAD_FILE";

const defaultState = {
  isVisible: false,
  files: [],
};

export const uploaderReducer = (state = defaultState, action) => {
  switch (action.type) {
    case SHOW_UPPLOADER: {
      return {
        ...state,
        isVisible: true,
      };
    }

    case HIDE_UPLOADER: {
      return {
        ...state,
        isVisible: false,
      };
    }

    case APPEND_UPLOAD_FILE: {
      return {
        ...state,
        files: [...state.files, action.payload],
      };
    }

    case REMOVE_UPLOAD_FILE: {
      return {
        ...state,
        files: [...state.files.filter((file) => file.id !== action.payload)],
      };
    }

    case CHANGE_UPLOAD_FILE: {
      return {
        ...state,
        files: [
          ...state.files.map((file) =>
            file.id === action.payload.id
              ? { ...file, progress: action.payload.progress }
              : { ...file }
          ),
        ],
      };
    }

    default: {
      return { ...state };
    }
  }
};

export const showUploader = () => ({ type: SHOW_UPPLOADER });
export const hideUpploader = () => ({ type: HIDE_UPLOADER });

export const appendUploadFile = (file) => ({
  type: APPEND_UPLOAD_FILE,
  payload: file,
});
export const removeUploadFile = (id) => ({
  type: REMOVE_UPLOAD_FILE,
  payload: id,
});
export const changeUploadFile = (data) => ({
  type: CHANGE_UPLOAD_FILE,
  payload: data,
});
